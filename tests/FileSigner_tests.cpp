#include <gtest/gtest.h>
#include <memory>
#include "Queue.hpp"
#include "FileSigner.hpp"


TEST(FileSigner, SimplTest) {
    auto input = std::make_shared<std::filesystem::path>("../data/test.txt");
    auto output = std::make_shared<std::filesystem::path>("./result.txt");
    
    FileSigner signer(input, output, 4096);
    auto result = signer.sign();
    ASSERT_EQ(result, true);
}


TEST(FileSigner, noFile) {
    auto input = std::make_shared<std::filesystem::path>("../data/NoCuschFile.txt");
    auto output = std::make_shared<std::filesystem::path>("./result.txt");
    
    FileSigner signer(input, output, 4096);
    auto result = signer.sign();
    ASSERT_EQ(result, false);
}

TEST(FileSigner, emptyFile) {
    auto input = std::make_shared<std::filesystem::path>("../data/empty.txt");
    auto output = std::make_shared<std::filesystem::path>("./emptyResult.txt");
    
    FileSigner signer(input, output, 4096);
    auto result = signer.sign();
    ASSERT_EQ(result, false);
}