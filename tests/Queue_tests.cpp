#include <gtest/gtest.h>
#include <memory>
#include "Queue.hpp"
#include "GreaterPtr.hpp"

using IdQueue = SafePriorityQueue<std::shared_ptr<size_t>, std::vector<std::shared_ptr<size_t>>, greaterPtr<std::shared_ptr<size_t>>> ;

std::shared_ptr<size_t> size_tPtr(size_t value) {
    return std::make_shared<size_t>(value);
}

TEST(QueueTest, SimplTest) {
    IdQueue queue;
    queue.push(size_tPtr(1));
    auto result = queue.top();
    ASSERT_EQ(result, std::nullopt);

    queue.push(size_tPtr(2));
    result = queue.top();
    ASSERT_EQ(result, std::nullopt);

    queue.push(size_tPtr(0));
    result = queue.top();
    ASSERT_EQ(*(result.value()), 0);

    result = queue.top();
    ASSERT_EQ(*(result.value()), 1);

    result = queue.top();
    ASSERT_EQ(*(result.value()), 2);

    result = queue.top();
    ASSERT_EQ(result, std::nullopt);
}

TEST(QueueTest, EmptyTest) {
    IdQueue queue;
    queue.push(size_tPtr(1));
    queue.push(size_tPtr(2));
    queue.push(size_tPtr(3));
    auto result = queue.top();
    ASSERT_EQ(result, std::nullopt);
}
