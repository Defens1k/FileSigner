#include <gtest/gtest.h>
#include <memory>
#include <cmath>
#include <future>
#include "WorkerPull.hpp"
#include "MD5.hpp"

std::string f(int input) { return std::to_string(input); }


TEST(WorkerPullTest, SimplTest) {
    int tasks = 1000;
    auto input = std::make_shared<SafeQueue<std::shared_ptr<std::packaged_task<std::string()>>>>();
    auto output = std::make_shared<SafeQueue<std::string>>() ;
    WorkerPull pull(input, output);
    for (int i = 0; i < tasks; i++) {
        input->push(std::make_shared<std::packaged_task<std::string()>>(std::bind(f, i)));
    }
    int count = 0;
    while(1) {
        auto result = output->top();
        if (result) {
            count++;
        }
        if (count == tasks) {
            break;
        }
    }
    ASSERT_EQ(count, tasks);
}
