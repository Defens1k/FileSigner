#include "FileSigner.hpp"
#include <fstream>

bool FileSigner::sign() {
    if (!std::filesystem::exists(*inputPath)) {
        std::cout << "File not found (" << std::filesystem::absolute(*inputPath) << ")\n";
        return false;
    }


    auto output = std::make_shared<std::ofstream>((*outputPath), std::ios::binary);
    if (!output->is_open()) {
        std::cout << "Can not open: " << (*outputPath) << std::endl;
        return false; 
    }

    auto fileSize = std::filesystem::file_size(*inputPath);
    if (fileSize <= 0) {
        std::cout << "File empty: " << (*inputPath) << std::endl;
        return false; 
    }  
    size_t tasks = fileSize / blockSize;
    if (fileSize % blockSize != 0) {
        tasks++;
    }
    auto input = std::make_shared<SafeQueue<std::shared_ptr<ReadTask>>>();
    auto scheduler = std::make_shared<Scheduler>();
    auto result = std::make_shared<DummyQueue<std::shared_ptr<Result>>>();
    result->setWaitLimit(tasks);

    WorkerPull<SafeQueue<std::shared_ptr<ReadTask>>, Scheduler> read(input, scheduler);

    WorkerPull write(scheduler, result, 1);
    std::cout << "tasks " << tasks << " filesize: " << fileSize << std::endl;

    for (size_t i = 0; i < tasks; i++) {
        input->push(std::make_shared<ReadTask>(inputPath, output, blockSize, i));
    }
    result->wait();
    auto errors = result->errors();
    bool resultFlag = true;
    for (auto i : errors) {
        std::cout << "Error: " << i << std::endl;
        resultFlag = false;
    }
    return resultFlag;
}