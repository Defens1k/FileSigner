#include "Tasks.hpp"
#include <fstream>

namespace task {


std::shared_ptr<WriteTask> read(std::shared_ptr<std::filesystem::path> input, 
            std::shared_ptr<std::ofstream> output, 
            size_t blockSize, size_t blockNumber) {
    std::ifstream file((*input), std::ios::binary);
    if (!file.is_open()) {
        auto errorMsg = std::make_shared<std::string>("Cannot read from file");
        return std::make_shared<WriteTask>(nullptr, nullptr, blockNumber, errorMsg);
    }
    file.seekg(blockSize * blockNumber);
    std::string s(blockSize, '\0');
    file.read(s.data(), blockSize);
    auto hash = std::make_shared<std::string>(MD5(s));

    return std::make_shared<WriteTask>(hash, output, blockNumber, std::nullopt);
}

std::shared_ptr<Result> write(std::shared_ptr<std::string> data,
                              std::shared_ptr<std::ofstream> output,
                              std::optional<Error> error) {
    if (error) {
        return std::make_shared<Result>(std::move(error));
    }
    try {
        (*output) << (*data) << std::endl;
        return std::make_shared<Result>();
    } catch (const std::exception & e) {
        return std::make_shared<Result>(Error(std::make_shared<std::string>(std::move(e.what()))));
    }
}


} //namespace task

ReadTask::ReadTask(std::shared_ptr<std::filesystem::path> input, 
                   std::shared_ptr<std::ofstream> output, 
                   size_t blockSize, size_t blockNumer) :
        task(std::bind(task::read, input, output, blockSize, blockNumer)) {};


WriteTask::WriteTask(std::shared_ptr<std::string> data, 
                     std::shared_ptr<std::ofstream> output, 
                     size_t blockNumber,
                     std::optional<Error> error) :
        priority(blockNumber),
        task(std::bind(task::write, data, output, error)) {};

