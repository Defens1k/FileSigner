#include <iostream>
#include <memory>
#include <vector>
#include <boost/program_options.hpp>
#include "FileSigner.hpp"

namespace po = boost::program_options;

int main(int argc, char ** argv) {
    std::string input;
    std::string output;
    size_t blockSize;
    // Configure options here
    po::options_description desc ("Allowed options");
    desc.add_options ()
        ("help,h", "print usage message")
        ("input,i", po::value(&input), "Input file")
        ("block_size,b", po::value<size_t>(&blockSize)->default_value(1048576), "Block size in bytes, [512..10485760], default 1048576")
        ("output,o", po::value(&output), "Output file");
    // Parse command line arguments
    po::variables_map vm;
    po::store (po::command_line_parser (argc, argv).options (desc).run (), vm);
    po::notify (vm);
    // Check if there are enough args or if --help is given
    if (vm.count ("help") || !vm.count ("input") || !vm.count ("output") || blockSize < 512 || blockSize > 10485760) {
        std::cerr << desc << "\n";
        return 1;
    }

    auto inputPtr = std::make_shared<std::filesystem::path>(input);
    auto outputPtr = std::make_shared<std::filesystem::path>(output);
    
    FileSigner signer(inputPtr, outputPtr, blockSize);
    auto result = signer.sign();
    if (result) {
        return 0;
    } else {
        return 2;
    }
}