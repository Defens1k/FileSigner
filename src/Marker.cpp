#include "Marker.hpp"


void Marker::wait() {
    semaphore.acquire();
}

void Marker::unmark() {
    //while (semaphore.try_acquire()) {};
    semaphore.try_acquire();
}

void Marker::mark() {
    semaphore.release();
}