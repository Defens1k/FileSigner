#include "MD5.hpp"
#include <iostream>
#include <algorithm>
#include <iterator>
#include <string>
#include <boost/uuid/detail/md5.hpp>
#include <boost/algorithm/hex.hpp>

using boost::uuids::detail::md5;

std::string toString(const md5::digest_type & digest) {
    const auto charDigest = reinterpret_cast<const char *>(&digest);
    std::string result;
    boost::algorithm::hex(charDigest, charDigest + sizeof(md5::digest_type), std::back_inserter(result));
    return result;
}

std::string MD5(const std::string & input) {
    md5 hash;
    md5::digest_type digest;

    hash.process_bytes(input.data(), input.size());
    hash.get_digest(digest);
    return toString(digest);
}