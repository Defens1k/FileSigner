#pragma once
#include <future>
#include <functional>
#include <filesystem>
#include "Queue.hpp"
#include "MD5.hpp"

class Error {
public: 
    Error(std::shared_ptr<std::string> message) : data(message) {};
    std::shared_ptr<std::string> message() {
        return data;
    }
private:
    std::shared_ptr<std::string> data = nullptr;
};

class Result {
public:
    Result(std::optional<Error> && value = std::nullopt): error(value){};
    bool hasError() {
        return error.has_value();
    }
    std::shared_ptr<std::string> errorMessage() {
        return error->message();
    }
private:
    std::optional<Error> error = std::nullopt;
};



class WriteTask {
public:
    WriteTask() = delete;
    WriteTask(std::shared_ptr<std::string> data, std::shared_ptr<std::ofstream> output, size_t blockNumber, std::optional<Error> error);
    bool operator ==(size_t other) {
        return other == priority;
    }
    bool operator >(const WriteTask & other) {
        return priority > other.priority;
    }
    void operator ()() {
        task();
    }
    size_t getPriority() {
        return priority;
    };
    std::future<std::shared_ptr<Result>> get_future() {
        return task.get_future();
    }
private:
    size_t priority = 0;
    std::packaged_task<std::shared_ptr<Result>()> task;
};



class ReadTask {
public:
    ReadTask(std::shared_ptr<std::filesystem::path> input, 
            std::shared_ptr<std::ofstream> output, 
            size_t blockSize, size_t blockNumer);
    std::future<std::shared_ptr<WriteTask>> get_future() {
        return task.get_future();
    }
    void operator ()() {
        task();
    }
private:
    std::packaged_task<std::shared_ptr<WriteTask>()> task;
};



