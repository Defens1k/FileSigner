#pragma once
template <typename T>
struct greaterPtr {
    constexpr bool operator()(const T & a, const T & b) const {
        return (*a) > (*b);
    }
};

