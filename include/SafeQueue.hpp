#pragma once
#include <queue>
#include <mutex>
#include "Marker.hpp"
#include "QueueBase.hpp"


template<class T>
class SafeQueue  {
public:
    SafeQueue() {};
    void push(T && value);
    void mark() {marker.mark();}
    std::optional<T> top();
private:
    std::mutex mutex;
    Marker marker;
    std::queue<T> queue;
};


template<class T>
void SafeQueue<T>::push(T && value) {
    mutex.lock();
    queue.push(value);
    marker.mark();
    mutex.unlock();
}
 
 
 
// template<class T>
// std::vector<T> SafeQueue<T>::topElements() {
//     marker.wait();
//     mutex.lock();
//     std::vector<T> result;
//     while (1) {
//         if (queue.empty()) {
//             break;
//         }
//         result.push_back(top);
//         queue.pop();
//     }
//     marker.unmark();
//     mutex.unlock();
//     return result;
// }

template<class T>
std::optional<T> SafeQueue<T>::top() {
    marker.wait();
    mutex.lock();
    std::optional<T> result = std::nullopt;
        
    if (!queue.empty()) {
        result = std::move(queue.front());
        queue.pop();
        marker.mark();
    } else {
        marker.unmark();
    }
    mutex.unlock();
    return result;
}

















