#pragma once
#include "Marker.hpp"
#include <atomic>
#include <set>

template<typename T>
class DummyQueue {
public:
    void push(T && value);
    void setWaitLimit(size_t value) {
        limit = value;
    };
    void wait() {
        marker.wait();
    };
    std::set<std::string> & errors() {
        return errorMessages;
    };
private:
    std::atomic<size_t> counter = 0;
    Marker marker;
    size_t limit = -1;
    std::set<std::string> errorMessages;
};


template <typename T>
void DummyQueue<T>::push(T && value) {
    counter++;
    if (counter >= limit) {
        marker.mark();
    }
    if (value->hasError()) {
        errorMessages.insert(std::move(*value->errorMessage()));
    }    
}
