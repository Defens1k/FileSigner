#pragma once
#include <string>
#include <filesystem>
#include "Queue.hpp"
#include "WorkerPull.hpp"
#include "Tasks.hpp"
#include "GreaterPtr.hpp"

using Scheduler = SafePriorityQueue<std::shared_ptr<WriteTask>, 
                                    std::vector<std::shared_ptr<WriteTask>>, 
                                    greaterPtr<std::shared_ptr<WriteTask>>>;



class FileSigner {
public:
    FileSigner() = delete;
    FileSigner(std::shared_ptr<std::filesystem::path> inputFile, 
               std::shared_ptr<std::filesystem::path> outputFile,
               size_t blockSize) :  
                                    inputPath(inputFile), 
                                    outputPath(outputFile),
                                    blockSize(blockSize) {};
    bool sign();
private:
    std::shared_ptr<std::filesystem::path> inputPath = nullptr;
    std::shared_ptr<std::filesystem::path> outputPath = nullptr;
    size_t blockSize = 0;
};


