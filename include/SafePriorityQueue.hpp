#pragma once
#include <queue>
#include <iostream>
#include <optional>
#include <mutex>
#include "Marker.hpp"

template<
    class T,
    class Container = std::vector<T>,
    class Compare = std::less<typename Container::value_type>
>
class SafePriorityQueue  {
public:
    SafePriorityQueue() {};
    void push(T && value);
    std::optional<T> top();
    void mark() {marker.mark();}
private:
    std::mutex mutex;
    Marker marker;
    std::priority_queue<T, Container, Compare> queue;
    size_t expectedTop = 0;
};


template<
    class T,
    class Container,
    class Compare
>
void SafePriorityQueue<T, Container, Compare>::push(T && value) {
    mutex.lock();
    queue.emplace(value);
    marker.mark();
    mutex.unlock();
}
 
template<
    class T,
    class Container,
    class Compare
>
std::optional<T> SafePriorityQueue<T, Container, Compare>::top() {
    marker.wait();
    mutex.lock();

    std::optional<T> result = std::nullopt;

    if (!queue.empty()) {
        const T & top = queue.top();
        if ((*top) == expectedTop) {
            result = std::move(top);
            queue.pop();
            expectedTop++;
            marker.mark();
        } else {
            marker.unmark();
        }
    } else {
        marker.unmark();
    }
    mutex.unlock();
    return result;
}
