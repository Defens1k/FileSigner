#pragma once
#include <semaphore>
#include <limits>
class Marker {
public:
    Marker() : semaphore(0) {};
    void wait();
    void unmark();
    void mark();
private:
    std::counting_semaphore<std::numeric_limits<int>::max()> semaphore;
};


class StopControl {
public:
    StopControl(): semaphore(0) {;};
    void start() {semaphore.release();};
    void go() {semaphore.acquire(); semaphore.release();};
    void stop() {semaphore.acquire();};
private:
    std::binary_semaphore semaphore;
};