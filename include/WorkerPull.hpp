#pragma once
#include "Queue.hpp"
#include <iostream>
#include <thread>
#include <memory>

using WorkerId = unsigned int;
constexpr static WorkerId kWorkersDefaultCount = 24;


template <typename InputQueue, typename ResultQueue>
class WorkerPull {
public:
    WorkerPull(std::shared_ptr<InputQueue>, std::shared_ptr<ResultQueue>, WorkerId count = kWorkersDefaultCount);
    ~WorkerPull();
private:
    WorkerId threadsCount = 0;
    std::shared_ptr<InputQueue> input = nullptr; 
    std::shared_ptr<ResultQueue> result = nullptr;
    std::vector<std::shared_ptr<std::thread>> threads;
    std::shared_ptr<std::atomic<bool>> descruct = nullptr;
};

template <typename InputQueue, typename ResultQueue>
WorkerPull<InputQueue, ResultQueue>::~WorkerPull() {
    (*descruct) = true;
    for (int i = 0; i < threadsCount * 2; i++) {
        input->mark();
    }
    for (int i = 0; i < threadsCount; i++) {
 //       std::cout << std::string("join " + std::to_string(i) + "\n");
        threads[i]->join();
    }   
}


template <typename InputQueue, typename ResultQueue>
WorkerPull<InputQueue, ResultQueue>::WorkerPull(std::shared_ptr<InputQueue> input, 
                       std::shared_ptr<ResultQueue> result,
                       WorkerId count) : 
                                input(input), result(result) {
    descruct = std::make_shared<std::atomic<bool>> (false);
    threadsCount = count;
    for (WorkerId i = 0; i < threadsCount; i++) {

        auto loop = [i, result, input,  descructPtr = descruct]() {

            while (1) {
                //std::cout << "thread loop" << std::endl;
                if ((*descructPtr)) {
                    return;
                }
                auto task = input->top();
                if (task.has_value()) {
                    auto localResult = (*task)->get_future();
                    (*(task.value()))();
                    result->push(localResult.get());
                }
            }
        };
        auto thread = std::make_shared<std::thread>(loop);
        threads.push_back(thread);
    }
}
